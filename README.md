# my-places

## Стек технологий

React, Redux, Typescript, Webpack, Ant Design, Vercel, Jest

_Линтеры:_ Stylelint, Eslint, Prettier

## Запуск проекта

Клонирование проекта

```bash
  git clone https://gitlab.com/BerdyevBK/my-places.git
```

Переходим в папку с проектом

```bash
  cd my-places
```

Устанавливаем зависимости

```bash
  npm install
```

Запускаем проект

```bash
  npm start
```

## Запуск тестов

Для запуска тестов

```bash
  npm run test
```
